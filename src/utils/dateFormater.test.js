import { dateFormater } from './dateFormater';

fdescribe('DateFormater', () => {
  it('should correct format date', () => {
      const day = 10, month = 2, year = 2019;
      const date = new Date(year, month, day);
      const formatDate = dateFormater(date);
      expect(formatDate).toBe(`${day}.${month + 1}.${year}`);
  });
});
