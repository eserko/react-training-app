const imgUrlPattern = /(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/;
const passwordPattern = /[\s\S]*?/;
const emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;

const required = value => value ? undefined : 'Required';
const mustBeNumber = value => isNaN(value) ? 'Must be a number' : undefined;
const minValue = min => value => isNaN(value) || value >= min ? undefined : `Should be greater than ${min}`;
const maxValue = max => value =>  isNaN(value) || value <= max ? undefined : `Should be smaller than ${max}`;
const minLength = min => value => value.length > min ? undefined : 'Too short descr';
const mustBeUrl = value => imgUrlPattern.test(value) ? undefined : 'Should be image url';
const validPassword = value => passwordPattern.test(value) ? undefined : 'Min 8 characters, min 1 letter and 1 number';
const validEmail = value => emailPattern.test(value) ? undefined : 'Should be email';

const composeValidators = (...validators) => value =>
  validators.reduce((error, validator) => error || validator(value), undefined);

export { required, mustBeNumber, minValue, maxValue, minLength, mustBeUrl, validPassword, validEmail, composeValidators };