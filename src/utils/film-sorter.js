import { sortTypes } from '../constants';

export const filmSorter = (films, param) => {
  if (param === sortTypes.vote_average) {
    films.sort((a, b) => b[param] - a[param]);
  } else {
    films.sort((a, b) => new Date(a[param]) - new Date(b[param]));
  }

  return films;
}

