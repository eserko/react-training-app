import { combineReducers } from 'redux';

import { filmsReducer } from './FilmsReducer';
import { authReducer } from './AuthReducer';
import { sharedReducer } from './SharedReducer';

export const rootReducer = combineReducers({
  filmState: filmsReducer,
  authState: authReducer,
  sharedState: sharedReducer,
});