import {
  LOG_IN,
  LOG_IN_SUCCESS,
  LOG_IN_FAILURE,
  LOG_OUT,
} from '../thunk-actions/AuthActionTypes';


const initialState = {
  isAuthenticated: false,
  userEmail: '',
  error: null
};

export function authReducer(state = initialState, action) {
  switch (action.type) {
    case LOG_IN:
      console.log('! logIn start');
      return {
        ...state,
      };

    case LOG_IN_SUCCESS:
      console.log('! logIn success');
      return {
        ...state,
        userEmail: action.payload,
        isAuthenticated: true,
      };

    case LOG_IN_FAILURE:
      console.log('! logIn error');
      return {
        ...state,
        error: action.payload,
      };

    case LOG_OUT:
      console.log('! logout');
      return {
        ...state,
        isAuthenticated: false,
      };

    default:
      return state;
  }
}