import { SHOW_SPINNER, HIDE_SPINNER } from '../actions/Shared/SharedActionTypes';

const initialState = {
  showSpinner: false,
};

export function sharedReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_SPINNER:
      console.log('show spinner');
      return {
        showSpinner: true,
      };
    case HIDE_SPINNER:
      console.log('hide spinner');
      return {
        showSpinner: false,
      };
    default:
      return state;
  }
};