import {
  SORT_FILMS,
  CLEAR_FILM
} from '../actions/FilmsActionTypes';

import {
  FETCH_FILMS,
  FETCH_FILMS_SUCCESS,
  FETCH_FILMS_FAILURE,
  FETCH_FILM,
  FETCH_FILM_SUCCESS,
  FETCH_FILM_FAILURE,
  SEARCH_FILMS,
  SEARCH_FILMS_SUCCESS,
  SEARCH_FILMS_FAILURE,
  ADD_FILM,
  ADD_FILM_SUCCESS,
  ADD_FILM_FAILURE,
  EDIT_FILM,
  EDIT_FILM_SUCCESS,
  EDIT_FILM_FAILURE,
  DELETE_FILM,
  DELETE_FILM_SUCCESS,
  DELETE_FILM_FAILURE,
} from '../thunk-actions/FilmsThunkActionTypes';


import { filmSorter } from '../../utils/film-sorter';

const initialState = {
  items: [],
  itemOpened: {},
  error: null
};

export function filmsReducer(state = initialState, action) {
  switch (action.type) {

    case FETCH_FILMS:
      console.log('! start fetch');
      return {
        ...state,
      };

    case FETCH_FILMS_SUCCESS:
      console.log('! fetch suceess');
      return {
        ...state,
        items: action.payload
      };

    case FETCH_FILMS_FAILURE:
      console.log('! fetch error');
      return {
        ...state,
        error: action.payload.error,
        items: []
      };

    case SEARCH_FILMS:
      console.log('! start search');
      return {
        ...state,
        items: [],
        error: null
      };

    case SEARCH_FILMS_SUCCESS:
      console.log('! search suceess');
      return {
        ...state,
        items: action.payload
      };

    case SEARCH_FILMS_FAILURE:
      console.log('! search error');
      return {
        ...state,
        error: action.payload.error,
        items: []
      };

    case SORT_FILMS:
      const filmsSorted = filmSorter(state.items, action.payload);

      return {
        ...state,
        items: [...filmsSorted]
      };

    // particular film actions

    case FETCH_FILM:
      console.log('! start fetch one film');
      return {
        ...state,
      };

    case FETCH_FILM_SUCCESS:
      console.log('! fetch success one film');
      return {
        ...state,
        itemOpened: action.payload
      };

    case FETCH_FILM_FAILURE:
      console.log('! fetch error one film');
      return {
        ...state,
        error: action.payload.error,
        itemOpened: {}
      };

    case ADD_FILM:
      console.log('! add film start');
      return {
        ...state,
      };

    case ADD_FILM_SUCCESS:
      console.log('! add film success');
      return {
        ...state,
      };

    case ADD_FILM_FAILURE:
      console.log('! add film error');
      return {
        ...state,
        error: action.payload.error,
      };

    case EDIT_FILM:
      console.log('! edit film start');
      return {};

    case EDIT_FILM_SUCCESS:
      console.log('! edit film success');
      return {};

    case EDIT_FILM_FAILURE:
      console.log('! edit film error');
      return {};

    case DELETE_FILM:
      console.log('! delete film start');
      return {};

    case DELETE_FILM_SUCCESS:
      console.log('! delete film success');
      return {};

    case DELETE_FILM_FAILURE:
      console.log('! delete film error');
      return {};

    case CLEAR_FILM:
      return {
        ...state,
        itemOpened: {}
      };

    default:
      return state;
  }

}

