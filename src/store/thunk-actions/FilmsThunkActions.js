import * as types from './FilmsThunkActionTypes';
import { SHOW_SPINNER, HIDE_SPINNER } from '../actions/Shared/SharedActionTypes';

import * as Constants from '../../constants';
import { db } from '../../configs/firebaseConfig';

export const fetchFilms = () => dispatch => {

  dispatch({ type: types.FETCH_FILMS });
  dispatch({ type: SHOW_SPINNER });

  db.collection('films').limit(Constants.limitItemsPerPage).get()
    .then(querySnapshot => {
      const data = querySnapshot.docs.map(doc => doc.data());

      dispatch({
        type: types.FETCH_FILMS_SUCCESS,
        payload: data
      });
      dispatch({ type: HIDE_SPINNER });
    })
    .catch(error => {
      dispatch({ type: types.FETCH_FILMS_FAILURE, payload: error });
      dispatch({ type: HIDE_SPINNER });
    });
};

export const searchFilms = (query, searchBy) => dispatch => {
  dispatch({ type: types.SEARCH_FILMS, payload: { query, searchBy } });
  dispatch({ type: SHOW_SPINNER });

  query = query.toLowerCase();

  if (searchBy === Constants.searchTypes.title) {
    db.collection('films').where('title', '==', query)
      .get()
      .then(querySnapshot => {
        const data = querySnapshot.docs.map(doc => doc.data());

        dispatch({
          type: types.SEARCH_FILMS_SUCCESS,
          payload: data
        });
        dispatch({ type: HIDE_SPINNER });
      })
      .catch(error => {
        dispatch({ type: types.SEARCH_FILMS_FAILURE, payload: error });
        dispatch({ type: HIDE_SPINNER });
      });
  } else {
    db.collection('films').where(Constants.searchTypes.genres, 'array-contains', query).limit(Constants.limitItemsPerPage)
      .get()
      .then(querySnapshot => {
        const data = querySnapshot.docs.map(doc => doc.data());

        dispatch({
          type: types.SEARCH_FILMS_SUCCESS,
          payload: data
        });
        dispatch({ type: HIDE_SPINNER });
      })
      .catch(error => {
        dispatch({ type: types.SEARCH_FILMS_FAILURE, payload: error });
        dispatch({ type: HIDE_SPINNER });
      });
  }
};

export const fetchFilm = (filmId) => dispatch => {
  dispatch({ type: types.FETCH_FILM, payload: filmId });

  const fllmRef = db.collection('films').doc(filmId);

  fllmRef.get()
    .then(doc => dispatch({
      type: types.FETCH_FILM_SUCCESS,
      payload: doc.data()
    })
    )
    .then(res => dispatch(
      searchFilms(res.payload.genres[0], Constants.searchTypes.genres)
    ))
    .catch(error => {
      dispatch({ type: types.FETCH_FILM_FAILURE, payload: error });
    });
};


export const editFilm = (film) => dispatch => {
  dispatch({ type: types.EDIT_FILM, payload: film });

  db.collection('films').doc(film.id).set(film)
    .then(film => {
      dispatch({
        type: types.EDIT_FILM_SUCCESS,
        payload: film
      });
    })
    .catch(error => {
      dispatch({ type: types.EDIT_FILM_FAILURE, payload: error });
    });
};

export const addFilm = (data, redirectFunc) => dispatch => {
  dispatch({ type: types.ADD_FILM, payload: data });
  dispatch({ type: SHOW_SPINNER });

  db.collection('films').add(data)
    .then(res => {
      const id = res.id;

      db.collection('films').doc(id)
        .update({ id: id });
    })
    .then(() => {
      dispatch({ type: types.ADD_FILM_SUCCESS });
      dispatch({ type: HIDE_SPINNER });
      redirectFunc();
    })
    .catch(error => {
      dispatch({ type: types.ADD_FILM_FAILURE, payload: error });
      dispatch({ type: HIDE_SPINNER });
    });
};

export const deleteFilm = (filmId, redirectFunc) => dispatch => {
  dispatch({ type: types.DELETE_FILM, payload: filmId });

  db.collection('films').doc(filmId).delete()
    .then(() => {
      dispatch({ type: types.DELETE_FILM_SUCCESS });
      redirectFunc();
    })
    .catch(error => {
      dispatch({ type: types.DELETE_FILM_FAILURE, payload: error });
    });
};