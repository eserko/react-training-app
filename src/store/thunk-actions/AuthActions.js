import * as types from './AuthActionTypes';
import { firebaseApp } from '../../configs/firebaseConfig';
import { ROUTES } from '../../constants';
import { HIDE_SPINNER, SHOW_SPINNER } from '../actions/Shared/SharedActionTypes';

export const logIn = ({ email, password }, history) => dispatch => {
  dispatch({ type: types.LOG_IN, paylaod: { email, password } });
  dispatch({ type: SHOW_SPINNER });

  firebaseApp
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(resp => {
      const { email } = resp.user;

      dispatch({ type: types.LOG_IN_SUCCESS, payload: email});
      dispatch({ type: HIDE_SPINNER });
      history.push(ROUTES.HOME);
    })
    .catch(error => {
      dispatch({ type: types.LOG_IN_FAILURE, payload: error.message });
      dispatch({ type: HIDE_SPINNER });
    });
};


export const logOut = () => dispatch => {
  firebaseApp
    .auth()
    .signOut();
  dispatch({ type: types.LOG_OUT });
};