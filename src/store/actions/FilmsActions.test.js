import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import * as actions from './FilmsActions';
import * as types from './FilmsActionTypes';
import * as Constants from '../../constants';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Films actions', () => {
  const store = mockStore({
    items: [],
    itemOpened: {},
  });

  const mockFilmId = '181808';
  const mockedFilm = {
    "id": 269149,
    "title": "Zootopia",
    "tagline": "Welcome to the urban jungle.",
    "vote_average": 7.7,
    "vote_count": 6795,
    "release_date": "2016-02-11",
    "poster_path": "https://image.tmdb.org/t/p/w500/sM33SANp9z6rXW8Itn7NnG1GOEs.jpg",
    "overview": "Determined to prove herself, Officer Judy Hopps, the first bunny on Zootopia's police force, jumps at the chance to crack her first case - even if it means partnering with scam-artist fox Nick Wilde to solve the mystery.",
    "budget": 150000000,
    "revenue": 1023784195,
    "genres": [
      "Animation",
      "Adventure",
      "Family",
      "Comedy"
    ],
    "runtime": 108
  };
  afterEach(() => {
    fetchMock.restore();
    store.clearActions();
  });

  describe('SYNC actions', () => {
    it('dispatches CLEAR_FILM when filmpage gets closed', () => {
      const expectedActions = [
        { type: types.CLEAR_FILM },
      ];

      store.dispatch(actions.clearFilm());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
})