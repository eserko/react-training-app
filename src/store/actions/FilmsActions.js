import * as types from './FilmsActionTypes';

export const sortFilms = (param) => dispatch => {
  dispatch({
    type: types.SORT_FILMS,
    payload: param,
  });
};

export const clearFilm = () => dispatch => {
  dispatch({ type: types.CLEAR_FILM });
};