import * as types from './SharedActionTypes';

export const showSpinner = () => dispatch => {
  dispatch({ type: types.SHOW_SPIINER });
};

export const hideSpinner = () => dispatch => {
  dispatch({ type: types.HIDE_SPIINER });
};
