import React from 'react';
import { connect } from 'react-redux';

export default function (ComposedComponent) {
  class Authentication extends React.Component {

    render() {
      const { isAuthenticated, history } = this.props;

      if (!isAuthenticated) {
        history.push('/login');
      }

      return (
        <div>
          {this.props.isAuthenticated && <ComposedComponent {...this.props} />}
        </div>
      );
    }
  }

  const mapStateToProps = store => {
    return { isAuthenticated: store.authState.isAuthenticated };
  };

  return connect(
    mapStateToProps,
  )(Authentication);
}