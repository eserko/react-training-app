import React from 'react';
import { connect } from 'react-redux';

export default function (ComposedComponent) {
  class NotAuthentication extends React.Component {

    render() {
      const { isAuthenticated, history } = this.props;
      
      if (isAuthenticated) {
        history.push('/');
      }

      return (
        <ComposedComponent {...this.props} />
      );
    }
  }

  const mapStateToProps = store => {
    return { isAuthenticated: store.authState.isAuthenticated };
  };

  return connect(
    mapStateToProps,
  )(NotAuthentication);
}