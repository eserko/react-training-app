import React from 'react';
import { Link } from 'react-router-dom';

import styles from './NotFound.module.css';


const NotFound = () => {
  return (
    <div>
      <h1>Not Found</h1>
      <span>it's better to get back to <Link to="/" className={styles.notfound_link}>Main page</Link></span>
    </div>
  );
};

export default NotFound;