import React from 'react';
import { Link } from 'react-router-dom';

import styles from '../../styles.css';

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <Link className={styles.app_logo} to="/">netflixroulette</Link>
        </footer>
    )
};

export default Footer