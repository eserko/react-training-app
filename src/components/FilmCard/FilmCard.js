import React from 'react';
import PropTypes from 'prop-types';

import styles from './FilmCard.module.css';
import { capitalize } from '../../utils/capitalize';

const genre = (genre) => (
  <span key={genre} className={styles.film_genre}>
    {capitalize(genre)}
  </span>
);

const FilmCard = (props) => {
  let { poster_path, title, release_date, genres, id } = props.data;
  let { onCardClick } = props;

  title = capitalize(title);
  release_date = new Date(release_date).getFullYear();

  return (
    <div onClick={() => onCardClick(id)}>
      <img src={poster_path} className={styles.poster}></img>
      <div className={styles.film_info}>
        <span className={styles.film_title}>{title}</span>

        <div><div className={styles.film_date}><span>{release_date}</span></div></div>
      </div>
      {genres.map(genre)}
    </div>
  );
};

FilmCard.defaultProps = {
  poster_path: '',
  title: '',
  release_date: '',
  id: '',
  genres: []
};

FilmCard.propTypes = {
  onClick: PropTypes.func,
  poster_path: PropTypes.string,
  title: PropTypes.string,
  release_date: PropTypes.string,
  id: PropTypes.string,
  genres: PropTypes.array
};


export default FilmCard;