import React from 'react';
import { connect } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
import { useAlert } from 'react-alert';

import styles from './Header.module.css';
import { navigationItems, alertMessages, ROUTES } from '../../constants';

import { logOut } from '../../store/thunk-actions/AuthActions';

import Button from '../Button/Button';

const Header = ({ logOut, auth: { userEmail, isAuthenticated } }) => {

  const alert = useAlert();

  const handleLogOut = () => { 
    logOut();
    alert.show(alertMessages.userLogOut);
  };

  const getLinks = (item) => {
    return (
      <NavLink
        to={item.path}
        activeClassName='active'
        key={item.path} >
        {item.text}
      </NavLink>
    );
  };

  const getUserLink = () => {
    if (isAuthenticated) {
      return (
        <div className={styles.auth_link}>
          <span className={styles.user_info}>{userEmail}</span>
          <Button onClick={handleLogOut} title='Log Out' classNames='delete_btn' />
        </div>
      );
    } else {
      return (
        <div className={styles.log_in}>
          <Link to={ROUTES.LOGIN}>Log In</Link>
        </div>
      );
    }
  };


  return (
    <header className={styles.header}>
      <Link className={styles.app_logo}  to={ROUTES.HOME}>netflixroulette</Link>
      <div className={styles.nav_links}>
        {navigationItems.map(getLinks)}
      </div>
      {getUserLink()}
    </header >
  );

}

const mapStateToProps = store => ({
  auth: store.authState,
});

const mapDispatchToProps = (dispatch) => ({
  logOut: () => dispatch(logOut())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);