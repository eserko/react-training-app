import React from 'react';
import Button from './Button';
import {shallow, mount} from 'enzyme';


describe('B U T T O N  Component', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = shallow(<Button />);
    });
  
    it('renders the button element', () => {
      expect(wrapper.find('button')).toBeTruthy();
    });

    it('should be called onClick', () => {
        const onClick = jest.fn();
        const component = mount(<Button onClick={onClick}/>);
        const btn = component.find('button').simulate('click');
        expect(onClick).toHaveBeenCalled();
    });
});
