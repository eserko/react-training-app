import React from 'react';
import PropTypes from 'prop-types';

import styles from './Button.module.css';

const Button = ( {title, onClick, classNames, disabled} ) => {

  const classes = () => {
    if (classNames instanceof Array) {
      return classNames.map((elem) => `${styles[elem]}`).join(' ');
    } else {
      return styles[classNames];
    }
  };
  
  return (
    <button
      type="submit"
      className={classes()}
      disabled={disabled}
      onClick={onClick}
    >
      {title}
    </button>
  );
};

Button.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func,
  classNames: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  title: 'button',
  classNames: '',
  disabled: false,
};

export default Button;