import React from 'react';

const NoResultsBlock = () => {
    return (
        <div >
            <p>No films found</p>
        </div>
    );
};

export default NoResultsBlock;