
import React from 'react';

import styles from './TextArea.module.css';

const TextArea = ({ title='', name='', rows=5, cols=55, value='', handleChange, placeholder='', className='', ...attr }) => (
  <>
    <label>{title}</label>
    <textarea
      name={name}
      rows={rows}
      cols={cols}
      value={value}
      onChange={handleChange}
      placeholder={placeholder} 
      className={styles[className]}
      {...attr}
      />
  </>
);

export default TextArea;