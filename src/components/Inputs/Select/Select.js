import React from 'react';

const Select = ({ name='', value='', title='', handleChange, placeholder='', options=[] }) => {
  return (
    <div>
      <label for={name}> {title} </label>
      <select
        id={name}
        name={name}
        value={value}
        onChange={handleChange}
      >
        <option value="" disabled>{placeholder}</option>
        {options.map(option => {
          return (
            <option
              key={option}
              value={option}
              label={option}>{option}</option>
          );
        })}
      </select>
    </div>)
}

export default Select;