
import React from 'react';

import styles from './CheckBox.module.css';

const CheckBox = ({ name='', title='', handleChange, selectedOptions=[], options=[], className='' }) => {

  return (
    <>
      <label htmlFor={name}>{title}</label>
      <div>
        {options.map(option => {
          return (
            <label key={option} className={styles.checkcontainer}>
              <input
                id={name}
                name={name}
                onChange={handleChange}
                value={option}
                type='checkbox'
                checked={selectedOptions.indexOf(option) > -1}
                type="checkbox" /> {option}
            </label>
          );
        })}
      </div>
    </>

  );

}

export default CheckBox;