import React from 'react';

const CheckboxGroup = ({ fields, options }) => {
  const toggle = (event, option) => {
    if (event.target.checked) { 
      fields.push(option);
    } else { 
      fields.remove(option); 
    };
  };

  return (
    <div>
      {options.map(option => (
        <label key={option}>
          <input
            type="checkbox"
            onClick={event => toggle(event, option)}
          />
          {option}
        </label>
      ))}
    </div>
  );
};

export default CheckboxGroup;