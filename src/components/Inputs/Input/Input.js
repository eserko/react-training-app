import React from 'react';
import PropTypes from 'prop-types';

import styles from './Input.module.css';

const Input = ({ name, title, type, className, handleChange, placeholder, disabled, showLabel, error, autoFocus, ...attr }) => {
  return (
    <>
      { showLabel && <label htmlFor={name}>{title}</label> }
      <input
        className={styles[className]}
        id={name}
        name={name}
        type={type}
        onChange={handleChange}
        placeholder={placeholder}
        autoFocus={autoFocus}
        {...attr} />
    </>
  );
};

Input.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  error: PropTypes.string,
  placeholder: PropTypes.string,
  handleChange: PropTypes.func,
  showLabel: PropTypes.bool,
};

Input.defaultProps = {
  name: '',
  className: '',
  title: '',
  disabled: false,
  autoFocus: false,
  type: '',
  error: '',
  placeholder: '',
  showLabel: true,
};

export default Input;