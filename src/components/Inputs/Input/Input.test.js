import React from 'react';
import { shallow } from 'enzyme';

import Input from './Input';

describe('Input Component', () => {
  // it('should render a placeholder', () => {
  //   const placeholderText = 'type anything here';
  //   const wrapper = shallow(<Input placeholder={placeholderText} />);
  //   expect(wrapper.prop('placeholder')).toEqual(placeholderText);
  // });
  const wrapper = shallow(<Input />);
  it('renders properly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});