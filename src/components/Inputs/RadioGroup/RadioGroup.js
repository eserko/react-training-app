import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RadioGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionSelected: props.optionSelected
    };
  }

  handleOptionChange = (e) => {
    const { handleChange } = this.props;
    const { value } = e.target;
    this.setState({ optionSelected:  value });

    if (handleChange) {
        handleChange(value);
    }
}

  render() {
    const { name, title, options } = this.props;

    return (
      <div>
        <label htmlFor={name} className="form-label">{title}</label>
          {options.map(option => {
            return (
              <label key={option}>
                <input
                  id={name}
                  name={name}
                  onChange={this.handleOptionChange}
                  value={option}
                  type="radio" 
                  checked={this.state.optionSelected === option}
                  /> {option}
              </label>
            );
          })}
      </div> 
    );
  }
}


RadioGroup.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  options: PropTypes.array,
  handleChange: PropTypes.func,
};

RadioGroup.defaultProps = {
  name: '',
  title: '',
  optionSelected: '',
  options: [],
};

export default RadioGroup;