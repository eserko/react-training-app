import Button from './Button/Button';
import ErrorBoundary from './ErrorBoundary/ErrorBoundary';
import FilmCard from './FilmCard/FilmCard';
import Footer from './Footer/Footer';
import Header from './Header/Header';
import NoResultsBlock from './NoResultsBlock/NoResultsBlock';
import NotFound from './NotFound/NotFound';
import ScrollToTop from './ScrollToTop/ScrollToTop';
import Spinner from './Spinner/Spinner';
import notRequiresAuth from './hoc/notRequiresAuth';
import requiresAuth from './hoc/requiresAuth';

export { 
  Button, 
  ErrorBoundary,
  FilmCard, 
  Footer,
  Header, 
  NoResultsBlock, 
  NotFound,
  ScrollToTop,
  Spinner, 
  notRequiresAuth,
  requiresAuth,
};