import React, { Component } from 'react';

import SearchForm from './SearchForm';

import { searchTypes } from '../../../../constants';

class SearchFormContainer extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    searchOptions: Object.keys(searchTypes),
    optionSelected: Object.keys(searchTypes)[0],
    searchQuery: ''
  }

  handleInputChange = (e) => {
    this.setState({ searchQuery: e.target.value });
  }

  handleRadioChange = (value) => {
    this.setState({ optionSelected: value });
  }

  handleFormSubmit = (e) => {
    e.preventDefault();

    this.props.searchFilms(this.state.searchQuery, this.state.optionSelected);
  }

  render() {
    const { searchOptions, optionSelected, searchQuery } = this.state;
    
    return (
      <SearchForm 
        handleFormSubmit={this.handleFormSubmit}
        handleInputChange={this.handleInputChange}
        handleRadioChange={this.handleRadioChange}
        searchQuery={searchQuery}
        searchOptions={searchOptions}
        optionSelected={optionSelected}
      />
    );

  }
}

export default SearchFormContainer;