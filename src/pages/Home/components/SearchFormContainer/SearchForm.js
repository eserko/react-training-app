import React from 'react';
import PropTypes from 'prop-types';

import { RadioGroup, Input } from '../../../../components/Inputs';
import Button from '../../../../components/Button/Button';

import styles from './SearchForm.module.css';
import globalStyles from '../../../../styles.css';

const SearchForm = ({ handleFormSubmit, handleInputChange, handleRadioChange, searchQuery = '', searchOptions = [], optionSelected = ''}) => {

  return (
    <form onSubmit={handleFormSubmit} className={styles.search_form}>
      <div className={globalStyles.mb_10}>
        <Input
          type='text'
          title='Search'
          name='search'
          value={searchQuery}
          placeholder='Search anything'
          handleChange={handleInputChange}
          className='search_input'
          autoFocus={true}
          showLabel={false}
        />
        <Button
          title='Search'
          classNames='search_btn'
        />
      </div>
      <RadioGroup
        title='Search by'
        name='options'
        options={searchOptions}
        optionSelected={optionSelected}
        handleChange={handleRadioChange}
      />
    </form>
  );

};

export default SearchForm;

SearchForm.propTypes = {
  handleFormSubmit: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  handleRadioChange: PropTypes.func.isRequired,
  searchQuery: PropTypes.string.isRequired,
  optionSelected: PropTypes.string.isRequired,
  searchOptions: PropTypes.arrayOf(PropTypes.string).isRequired,
};