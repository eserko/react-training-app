import React from 'react';
import { shallow, mount } from 'enzyme';

import SearchForm from './SearchForm';

describe('SearchForm Component', () => {
  const mockSearchFilms = jest.fn();

  const props = {
    handleInputChange: () => {},
    handleRadioChange: () => {},
    handleFormSubmit: () => {},
    searchQuery: '',
    optionSelected: '',
    searchOptions: [''],

  };

  const component = shallow(<SearchForm {...props} />);

  it('renders properly', () => {
    expect(component).toMatchSnapshot();
  });

  describe('Form handlers', () => {
    describe('when typing into search input', () => {

      const component = mount(<SearchForm {...props} />);

      const query = 'test query';

      beforeEach(() => {
        component.find('#search').simulate('change', {
          target: {
            value: query,
          },
        });
      });
    });
  });
});
