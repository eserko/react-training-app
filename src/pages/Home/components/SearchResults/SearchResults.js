import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import { FilmCard, Spinner, NoResultsBlock } from '../../../../components';
import { RadioGroup } from '../../../../components/Inputs';

import { sortTypes } from '../../../../constants';
import globalStyles from '../../../../styles.css';
import styles from './SearchResults.module.css';

const sortOptions = Object.keys(sortTypes);

const SearchResults = ({ films, sortFilms, showSpinner, history }) => {

  const handleCardClick = (filmId) => {
    history.push(`/film/${filmId}`);
  };

  const handleRadioChange = (value) => {
    sortFilms(value);
  };

  const getItem = (item, index) => (
    <div className={globalStyles.film_card} key={index}>
      <FilmCard data={item} onCardClick={handleCardClick} />
    </div>
  );

  return (
    <>
      <div className={styles.search_footer}>
        <span>Films found: {films.length}</span>
        <div className={globalStyles.mb_20}>
          <RadioGroup
            title={'Sort by'}
            name={'sort-type'}
            options={sortOptions}
            handleChange={handleRadioChange}
          />
        </div>
      </div>

      {showSpinner && <Spinner />}

      {!films.length && !showSpinner
        ? <NoResultsBlock />
        : <div className={globalStyles.search_results}> {films.map(getItem)} </div>
      }
    </>
  );
};

SearchResults.propTypes = {
  films: PropTypes.array,
  showSpinner: PropTypes.bool,
  sortFilms: PropTypes.func,
};

SearchResults.defaultProps = {
  films: [],
  showSpinner: false,
};

export default withRouter(SearchResults);
