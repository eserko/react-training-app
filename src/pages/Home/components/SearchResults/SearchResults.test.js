import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { shallow, mount } from 'enzyme';

import SearchResults from './SearchResults';
import { FilmCard, NoResultsBlock } from '../../../../components';


describe('SearchResults component', () => {

  describe('SearchResults with data', () => {
    let films;

    beforeEach(() => {
      films = [
        {
          "id": 181808,
          "title": "Star Wars: The Last Jedi",
          "tagline": "The Saga Continues",
          "vote_average": 7.1,
          "vote_count": 4732,
          "release_date": "2017-12-13",
          "poster_path": "https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg",
          "overview": "Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.",
          "budget": 200000000,
          "revenue": 1325937250,
          "genres": [
            "Fantasy",
            "Adventure",
            "Science Fiction"
          ],
          "runtime": 152,
        },
        {
          "id": 284054,
          "title": "Black Panther",
          "tagline": "Long live the king",
          "vote_average": 7.3,
          "vote_count": 3788,
          "release_date": "2018-02-13",
          "poster_path": "https://image.tmdb.org/t/p/w500/uxzzxijgPIY7slzFvMotPv8wjKA.jpg",
          "overview": "King T'Challa returns home from America to the reclusive, technologically advanced African nation of Wakanda to serve as his country's new leader. However, T'Challa soon finds that he is challenged for the throne by factions within his own country as well as without.  Using powers reserved to Wakandan kings, T'Challa assumes the Black Panther mantel to join with girlfriend Nakia, the queen-mother, his princess-kid sister,  members of the Dora Milaje (the Wakandan \"special forces\"), and an American secret agent, to prevent Wakanda from being dragged into a world war.",
          "budget": 200000000,
          "revenue": 1245257672,
          "genres": [
            "Action",
            "Adventure",
            "Fantasy",
            "Science Fiction"
          ],
          "runtime": 134
        },

      ];
    });

    it('should render two elements', () => {
      const component = shallow(<SearchResults films={films} />);
      expect(component).toMatchSnapshot();
    });

    it('should render two components - FilmCard', () => {
      const component = mount(
        <BrowserRouter>
          <SearchResults films={films} />
        </BrowserRouter>
      );
      expect(component.find(FilmCard).length).toBe(2);
    });
  });


  describe('SearchResults with no data', () => {
    const filmsEmpty = [];
    const showSpinner = false;
    const component = mount(
      <BrowserRouter>
        <SearchResults films={filmsEmpty} showSpinner={showSpinner}/>
      </BrowserRouter>
    );

    it('should render NoResultsBlock"', () => {
      expect(component.find(NoResultsBlock).length).toBe(1);
    });
  });
});