import React, { Component } from 'react';
import { connect } from 'react-redux';

import { sortFilms } from '../../store/actions/FilmsActions';
import { fetchFilms, searchFilms } from '../../store/thunk-actions/FilmsThunkActions';

import { SearchFormContainer, SearchResults } from './components';

class Home extends Component {

  componentDidMount() {
    this.props.fetchFilms();
  }

  sortFilms = (sortParam) => {
    this.props.sortFilms(sortParam);
  }

  searchFilms = (query, option) => {
    this.props.searchFilms(query, option);
  }

  render() {
    const { films, showSpinner } = this.props;
    return (
      <>
        <SearchFormContainer searchFilms={this.searchFilms} />
        <SearchResults films={films} showSpinner={showSpinner} sortFilms={this.sortFilms} />
      </>
    );
  }
}

const mapStateToProps = store => ({
  films: store.filmState.items,
  showSpinner: store.sharedState.showSpinner,
});

const mapDispatchToProps = dispatch => ({
  fetchFilms: () => dispatch(fetchFilms()),
  sortFilms: (param) => dispatch(sortFilms(param)),
  searchFilms: (query, searchBy) => dispatch(searchFilms(query, searchBy))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);