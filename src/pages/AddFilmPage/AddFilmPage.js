import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useAlert } from 'react-alert';

import { Form, Field } from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import { FieldArray } from 'react-final-form-arrays';

import { genresOptions, formTextInputs, formTextareaInput, alertMessages, ROUTES } from '../../constants';
import { required, mustBeNumber, minValue, maxValue, minLength, mustBeUrl, composeValidators } from '../../utils/validate';

import { addFilm } from '../../store/thunk-actions/FilmsThunkActions';
import { Input, TextArea, CheckboxGroup } from '../../components/Inputs';
import { Button } from '../../components';

import styles from './AddFilmPage.module.css';

const AddFilmPage = ({ addFilm }) => {

  const [genresError, setGenresError] = useState(false);

  const alert = useAlert();

  const handleFormSubmit = (data) => {
    if (data.genres && data.genres.length) {
      setGenresError(false);
      addFilm(data);
      alert.show(alertMessages.filmAdded);
    } else {
      setGenresError(true);
    }
  };

  const getTextInputs = (item) => {
    let validateRules = item.type === 'number' ? composeValidators(required, mustBeNumber, minValue(item.minvalue), maxValue(item.maxvalue)) :
      item.name === 'poster_path' ? composeValidators(required, mustBeUrl) : required;

    return (
      <div key={item.title}>
        <Field name={item.name} validate={validateRules}>
          {
            ({ input, meta }) => {
              let props = { ...input, ...item };
              return (
                <div className={styles.input_wrapper}>
                  <Input {...props} />
                  {meta.error && meta.touched && <span className={styles.error}>{meta.error}</span>}
                </div>
              );
            }
          }
        </Field>
      </div>
    );
  };

  return (
    <Form
      onSubmit={handleFormSubmit}
      mutators={{ ...arrayMutators }}
      render={({ handleSubmit, form, submitting, pristine }) => (
        <form onSubmit={handleSubmit} className={styles.film_form}>

          {formTextInputs.map(getTextInputs)}

          <Field name='overview' validate={composeValidators(required, minLength(formTextareaInput.minLength))} type='textarea'>
            {
              ({ input, meta }) => {
                let props = { ...input, ...formTextareaInput };
                return (
                  <div className={styles.input_wrapper}>
                    <TextArea {...props} />
                    {meta.error && meta.touched && <span className={styles.error}>{meta.error}</span>}
                  </div>
                );
              }
            }
          </Field>

          <div className={styles.checkbox_block}>
            <FieldArray
              name="genres"
              component={CheckboxGroup}
              options={genresOptions}
            />
            {genresError && <span className={styles.error}>Chose one at least</span>}
          </div>

          <div className={styles.form_btns}>
            <Button
              title='Clear form'
              onClick={form.reset}
              classNames='clear_btn'
              disabled={submitting || pristine}
            />

            <Button
              title='Submit'
              classNames='submit_btn'
              onClick={handleSubmit}
              disabled={submitting}
            />
          </div>
        </form>
      )
      }
    />
  );
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  addFilm: (film) => {
    dispatch(addFilm(film, () => ownProps.history.push(ROUTES.HOME)));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(AddFilmPage);
