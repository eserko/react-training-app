import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Form, Field } from 'react-final-form';

import { logIn } from '../../store/thunk-actions/AuthActions';

import { Button, Spinner } from '../../components';
import { Input } from '../../components/Inputs';

import { loginFormInputs } from '../../constants';
import { required, validEmail, validPassword, composeValidators } from '../../utils/validate';

import styles from './LoginPage.module.css';
import globalStyles from '../../styles.css';

const LoginPage = ({ logIn, history, authError, showSpinner }) => {

  const [isErrorDisplayed, setErrorDisplay] = useState(false);

  const handleFormSubmit = (data) => {
    logIn(data, history);
    setErrorDisplay(true);
  };

  const getInputs = (item) => {
    const validateRules = item.type === 'password'
      ? composeValidators(required, validPassword)
      : composeValidators(required, validEmail);

    return (
      <div key={item.title} className={styles.input_wrapper}>
        <Field name={item.name} validate={validateRules}>
          {
            ({ input, meta }) => {
              let props = { ...input, ...item };
              return (
                <>
                  <Input {...props} />
                  <div>
                    {meta.error && meta.touched && <span className={styles.error}>{meta.error}</span>}
                  </div>
                </>
              );
            }
          }
        </Field>
      </div>
    );
  };

  const getError = () => {
    if (authError && isErrorDisplayed) {
      return <div className={styles.error}>{authError}</div>;
    }
  };


  return (
    <>
      <h1>Log in to use the app</h1>
      <Form
        onSubmit={handleFormSubmit}
        render={({ handleSubmit, submitting }) => (
          <form
            onSubmit={handleSubmit}
            onChange={() => setErrorDisplay(false)}
            className={styles.login_form}>

            {loginFormInputs.map(getInputs)}

            <div className={globalStyles.mb_20}>
              <Button
                title='Log in'
                classNames={['submit_btn', 'wide']}
                onClick={handleSubmit}
                disabled={submitting}
              />
            </div>
          </form>
        )
        }
      />
      {showSpinner ? <Spinner/> : getError()}
    </>

  );

};

const mapStateToProps = store => ({
  authError: store.authState.error,
  showSpinner: store.sharedState.showSpinner,
});

const mapDispatchToProps = (dispatch) => ({
  logIn: (data, history) => dispatch(logIn(data, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
