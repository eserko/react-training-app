import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import FilmPage from './FilmPage';


const mockStore = configureStore([]);
const film = {
  'id': 269149,
  'title': 'Zootopia',
  'tagline': 'Welcome to the urban jungle.',
  'vote_average': 7.7,
  'vote_count': 6795,
  'release_date': '2016-02-11',
  'poster_path': 'https://image.tmdb.org/t/p/w500/sM33SANp9z6rXW8Itn7NnG1GOEs.jpg',
  'overview': 'Determined to prove herself, Officer Judy Hopps, the first bunny on Zootopia\'s police force, jumps at the chance to crack her first case - even if it means partnering with scam-artist fox Nick Wilde to solve the mystery.',
  'budget': 150000000,
  'revenue': 1023784195,
  'genres': [
    'Animation',
    'Adventure',
    'Family',
    'Comedy'
  ],
  'runtime': 108
};

describe('F I L M P A G E Component', () => {
  const deleteFilm = jest.fn();
  let store;
  let component;

  beforeEach(() => {
    store = mockStore({
      filmState: {
        itemOpened: film,
      },
      sharedState: {
        showSpinner: false,
      },
      deleteFilm: deleteFilm,
    });

    component = shallow(
      <Provider store={store}>
        <FilmPage />
      </Provider>
    );
  });

  it('renders properly', () => {
    expect(component).toMatchSnapshot();
  });

});