import React from 'react';
import PropTypes from 'prop-types';

import styles from './Film.module.css';

import { capitalize } from '../../../../utils/capitalize';

const Film = ({ film: { poster_path = '', title = '', release_date = '', overview = '', tagline = '' } }) => {
  title = capitalize(title);

  return (
    <div className={styles.film_block}>
      <img src={poster_path} className={styles.poster}></img>
      <div className={styles.descr_block}>
        <h1>{title}</h1>
        <p>{tagline}</p>
        <p>{overview}</p>
        <p>{release_date}</p>
      </div>
    </div>
  );
};

Film.propTypes = {
  film: PropTypes.shape({
    poster_path: PropTypes.string,
    title: PropTypes.string,
    release_date: PropTypes.string,
    overview: PropTypes.string,
    tagline: PropTypes.string,
  })
};

export default Film;