import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Film from './components/Film/Film';
import { FilmCard, Spinner, NoResultsBlock, Button } from '../../components';

import { clearFilm } from '../../store/actions/FilmsActions';
import { fetchFilm, deleteFilm } from '../../store/thunk-actions/FilmsThunkActions';

import styles from '../../styles.css';
import { capitalize } from '../../utils/capitalize';
import { ROUTES } from '../../constants';

class FilmPage extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { clearFilm, match: { params: { id } } } = this.props;

    clearFilm();
    this.fetchFilm(id);
  }

  fetchFilm = (filmId) => {
    const { fetchFilm } = this.props;

    fetchFilm(filmId);
  }

  handleCardClick = (filmId) => {
    this.props.history.push(`/film/${filmId}`);

    this.fetchFilm(filmId);
  }

  getItem = (item) => {
    const { film: { id } } = this.props;

    if (item.id !== id) {
      return (
        <div className={styles.film_card} key={item.id}>
          <FilmCard data={item} onCardClick={this.handleCardClick} />
        </div>
      );
    }
  }

  getFilmsByGenre = () => {
    const { filmsByGenre, showSpinner } = this.props;

    if (showSpinner) return <Spinner />;

    return filmsByGenre.length
    ? <div className={styles.search_results}> {filmsByGenre.map(this.getItem)} </div>
    : <NoResultsBlock />;
  }

  render() {
    const { film, deleteFilm } = this.props;
    const filmGenre = film.genres ? capitalize(film.genres[0]) : '';

    if (!Object.getOwnPropertyNames(film).length) return <Spinner />;

    return (
      <>
        <div className={styles.container}>
          {film.title && <Film film={film} />}
        </div>
        <Button onClick={deleteFilm(film.id)} title='Delete Film' classNames='delete_btn' />
        <div className={styles.mb_20 + ' ' + styles.mt_20}>Films by <span className={styles.text_bold}>{filmGenre}</span> genre</div>
        {this.getFilmsByGenre()}
      </>
    );
  }
}

FilmPage.propTypes = {
  film: PropTypes.object,
  filmsByGenre: PropTypes.array,
  showSpinner: PropTypes.bool,
};

FilmPage.defaultProps = {
  film: {},
  filmsByGenre: [],
  showSpinner: false,
};


const mapStateToProps = store => ({
  film: store.filmState.itemOpened,
  filmsByGenre: store.filmState.items,
  showSpinner: store.sharedState.showSpinner
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchFilm: (param) => dispatch(fetchFilm(param)),
  clearFilm: () => dispatch(clearFilm()),
  deleteFilm: (param) => {
    return () => {
      dispatch(deleteFilm(param, () => ownProps.history.push(ROUTES.HOME)));
    };
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilmPage);