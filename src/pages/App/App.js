import React, { useEffect } from 'react';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundary';

import { firebaseApp } from '../../configs/firebaseConfig';
import { store } from '../../store/configStore';
import { LOG_IN_SUCCESS } from '../../store/thunk-actions/AuthActionTypes';

import styles from '../../styles.css';

const App = ({ children }) => {

  useEffect(() => {
    firebaseApp.auth().onAuthStateChanged(authUser => {
      if (authUser) {
        store.dispatch({ type: LOG_IN_SUCCESS, payload: authUser.email });
      }
    });
  });

  return (
    <ErrorBoundary>
      <div className={styles.wrapper}>
        <Header />
        <div className={styles.content}>
          {children}
        </div>
        <Footer />
      </div>
    </ErrorBoundary>
  );
};

export default App;