import AddFilmPage from './AddFilmPage/AddFilmPage';
import FilmPage from './FilmPage/FilmPage';
import LoginPage from './LoginPage/LoginPage';
import Home from './Home/Home';
import App from './App/App';

export { 
  AddFilmPage,
  FilmPage,
  LoginPage,
  Home,
  App
};