import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';

import { store } from './store/configStore';

import { App, Home, FilmPage, AddFilmPage, LoginPage } from './pages';
import { ScrollToTop, NotFound, notRequiresAuth, requiresAuth } from './components';

import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

import { alertTmpltOptions, ROUTES } from './constants';

import './styles.css';


ReactDOM.render((
  <Provider store={store}>
      <BrowserRouter>
        <ScrollToTop>
          <AlertProvider template={AlertTemplate} {...alertTmpltOptions}>
            <App>
              <Switch>
                <Route exact path={ROUTES.HOME} component={Home} />
                {/* <Route path='/home' component={Home} /> */}
                <Route path={ROUTES.FILM} component={FilmPage} />
                <Route path={ROUTES.ADD_FILM}  component={requiresAuth(AddFilmPage)} />
                <Route path={ROUTES.LOGIN} component={notRequiresAuth(LoginPage)} />
                {/* <Route path='*' render={() => (<Redirect to='/' />)} /> */}
                <Route path='*' component={NotFound} />
              </Switch>
            </App>
          </AlertProvider>
        </ScrollToTop>
      </BrowserRouter>
  </Provider>
), document.getElementById('root'));

registerServiceWorker();