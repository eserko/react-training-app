import firebase from 'firebase';
import 'firebase/auth';

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyCp-nVnW-bjy16LmQLlvQL0DNEGpuuQ2a8',
  authDomain: 'films-4158c.firebaseapp.com',
  databaseURL: 'https://films-4158c.firebaseio.com',
  projectId: 'films-4158c',
  storageBucket: '',
  messagingSenderId: '107152607278',
  appId: '1:107152607278:web:81c10fbdf1bb5080'
});

const db = firebaseApp.firestore();
export { firebaseApp, db };