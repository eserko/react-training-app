export const sortTypes = {
  vote_average: 'vote_average',
  release_date: 'release_date',
};

export const searchTypes = {
  title: 'title',
  genres: 'genres',
};

export const navigationItems = [
  { path: '/', text: 'Home' },
  { path: '/add-film', text: 'Add Film' }
];

export const limitItemsPerPage = 10;

export const genresOptions = ['fantasy', 'adventure', 'action', 'comedy', 'thriller', 'animation'];

export const formTextInputs = [
  {
    title: 'Title',
    name: 'title',
    placeholder: 'Film title',
    type: 'text',
    className: 'add_film'
  },
  {
    title: 'Tagline',
    name: 'tagline',
    placeholder: 'Film tagline',
    type: 'text',
    className: 'add_film',
  },
  {
    title: 'Poster',
    name: 'poster_path',
    placeholder: 'Poster link',
    type: 'text',
    className: 'add_film'
  },
  {
    title: 'Release year',
    name: 'release_date',
    placeholder: 'Release year',
    type: 'number',
    className: 'add_film',
    minvalue: 1910,
    maxvalue: 2019
  },
  {
    title: 'Average vote',
    name: 'vote_average',
    placeholder: 'Average vote',
    type: 'number',
    className: 'add_film',
    minvalue: 1,
    maxvalue: 10
  },
];

export const formTextareaInput =  {
  placeholder: 'Film Overview',
  className: 'add_film',
  title: 'Overview',
  minLength: 20,
};

export const loginFormInputs = [
  {
    title: 'Email',
    name: 'email',
    placeholder: 'Your Email',
    type: 'text',
    showLabel: false,
  },
  {
    title: 'Password',
    name: 'password',
    placeholder: 'Password',
    type: 'password',
    showLabel: false,
  }
];

export const alertTmpltOptions = {
  position: 'bottom right',
  timeout: 4000,
  offset: '30px',
  transition: 'scale',
  type: 'info',
};

export const alertMessages = {
  filmDeleted: 'Film was deleted',
  filmAdded: 'Film was added',
  userLogOut: 'You\'ve logged out',
};

export const ROUTES = {
  HOME: '/',
  ADD_FILM: '/add-film',
  FILM: '/film/:id',
  LOGIN: '/login',
};
